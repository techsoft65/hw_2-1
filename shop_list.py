
def print_help():
    my_commands = {
        'ld': ['list_dishes',
               'команда, которая выводит список блюд;'],
        'li': ['list_ingredients', 'команда, которая выведет рецепт по блюду'],
        'sl': ['get_shop_list_by_dishes', 'команда, которая выведет \
        список покупок по указанным блюдам и количеству гостей']
    }
    for k, v in my_commands.items():
        print('{} - {}'.format(k, v[1]))


def print_list_dishes(cook_book):
    print('Список блюд:')
    for i, k in enumerate(cook_book.keys()):
        print('{}. {}'.format(i+1,k))


def print_list_ingredients(cook_book):
    dish = str(input('Введите блюдо, по которому требуется вывести рецепт:'))
    for ingredients in cook_book[dish]:
        print('{} - {} - {}'.format(ingredients['ingredient_name'], ingredients['quantity'], ingredients['measure'],))


def get_shop_list_by_dishes(cook_book, dishes, person_count):
    shop_list = {}
    for dish in dishes:
        for ingredient in cook_book[dish]:
            new_shop_list_item = dict(ingredient)
            new_shop_list_item['quantity'] *= person_count
            if new_shop_list_item['ingredient_name'] not in shop_list:
                shop_list[new_shop_list_item['ingredient_name']] = new_shop_list_item
            else:
                shop_list[new_shop_list_item['ingredient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list


def print_shop_list(shop_list):
    for shop_list_item in shop_list.values():
        print('{} {} {}'.format(shop_list_item['ingredient_name'], shop_list_item['quantity'], shop_list_item['measure']))


def create_shop_list(cook_book):
    person_count = int(input('Введите количество человек: '))
    dishes = input('Введите блюда в расчете на одного человека (через запятую): ').lower().split(', ')
    shop_list = get_shop_list_by_dishes(cook_book, dishes, person_count)
    print_shop_list(shop_list)


def read_cook_book():
    cook_book = {}
    with open('recipes_data.txt') as f:
        for line in f:
            dish = line.strip()
            n = f.readline().strip()
            my_list_ingredients = []
            for i in range(int(n)):
                line = f.readline().strip()
                ingredients = {}
                ingredient = line.split(' | ')
                ingredients['ingredient_name'] = ingredient[0]
                ingredients['quantity'] = int(ingredient[1])
                ingredients['measure'] = ingredient[2]
                my_list_ingredients.append(ingredients)
            # print('Блюдо {}\nколичество инградиентов {}\nсписок ингадиентов {}'.format(dish, n, list_ingredients))
            f.readline()
            cook_book[dish] = my_list_ingredients
    return cook_book


def main():
    my_cook_book = read_cook_book()

    print('Добро пожаловать в программу, "Ничего не забыть в магазине 1.0"! ')
    print('Для получения справки по командам нажмите "help", для выхода из программы "exit"')
    key = ''
    while key != 'exit':
        key = str(input('Введите команду:'))
        if key == 'ld':
            print_list_dishes(my_cook_book)
        elif key == 'li':
            print_list_ingredients(my_cook_book)
        elif key == 'sl':
            create_shop_list(my_cook_book)
        elif key == 'help':
            print_help()

    print('Спасибо, что использовали программу!')


main()

# new_list = [int(i) for i in splitted]   лист камприкэйшен
# print(f.readline(), end='')
